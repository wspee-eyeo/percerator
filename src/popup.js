
document.addEventListener('DOMContentLoaded', () => {

  // get the filterText for the current page from the background page
  chrome.tabs.query({active:true, lastFocusedWindow: true}, (tabs) => {
    for (let tab of tabs)
    {
      chrome.runtime.sendMessage(
          {action: "getFilterText", url: tab.url}, (response) => {
        if (response.filterText)
          document.querySelector('#hashes').innerText = response.filterText;
      });
    }
  });

  let clipboardWrite = (filterText) => {
    navigator.clipboard.writeText(filterText).catch(console.log);
  };

  document.querySelector('#copy').onclick = (event) => {
    chrome.tabs.query({active:true, lastFocusedWindow: true}, (tabs) => {
      for (let tab of tabs)
        chrome.runtime.sendMessage(
            {action: "getFilterText", url: tab.url}, (response) => {
          clipboardWrite(response.filterText);
        });
    });
  };

  document.querySelector('#copyAndClear').onclick = (event) => {
    chrome.tabs.query({active:true, lastFocusedWindow: true}, (tabs) => {
      for (let tab of tabs)
        chrome.runtime.sendMessage(
            {action: "getFilterText", clear: true, url: tab.url},
            (response) => {
          clipboardWrite(response.filterText);
          document.querySelector('#hashes').innerText = "Empty";
        });
    });
  };
});
