/* The following lines are based off the blockhash-js library which is
 * licences under the MIT licence
 * https://github.com/commonsmachinery/blockhash-js/tree/
 *  2084417e40005e37f4ad957dbd2bca08ddc222bc */
// Start blockhash-js
let oneBits = [0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4];

// Calculate the hamming distance for two hashes in hex format
let hammingDistance = function(hash1, hash2)
{
  let d = 0;
  let i;

  if (hash1.length !== hash2.length)
  {
    throw new Error("Can't compare hashes with different length");
  }

  for (i = 0; i < hash1.length; i++)
  {
    let n1 = parseInt(hash1[i], 16);
    let n2 = parseInt(hash2[i], 16);
    d += oneBits[n1 ^ n2];
  }
  return d;
};

let median = function(data)
{
  let mdarr = data.slice(0);
  mdarr.sort((a, b) => { return a - b; });
  if (mdarr.length % 2 === 0)
  {
    return (mdarr[mdarr.length / 2 - 1] + mdarr[mdarr.length / 2]) / 2.0;
  }
  return mdarr[Math.floor(mdarr.length / 2)];
};

let translateBlocksToBits = function(blocks, pixelsPerBlock)
{
  let halfBlockValue = pixelsPerBlock * 256 * 3 / 2;
  let bandsize = blocks.length / 4;

  // Compare medians across four horizontal bands
  for (let i = 0; i < 4; i++)
  {
    let m = median(blocks.slice(i * bandsize, (i + 1) * bandsize));
    for (let j = i * bandsize; j < (i + 1) * bandsize; j++)
    {
      let v = blocks[j];

      // Output a 1 if the block is brighter than the median.
      // With images dominated by black or white, the median may
      // end up being 0 or the max value, and thus having a lot
      // of blocks of value equal to the median.  To avoid
      // generating hashes of all zeros or ones, in that case output
      // 0 if the median is in the lower value space, 1 otherwise
      blocks[j] = Number(v > m ||
        (Math.abs(v - m) < 1 && m > halfBlockValue));
    }
  }
};

let bitsToHexhash = function(bitsArray)
{
  let hex = [];
  for (let i = 0; i < bitsArray.length; i += 4)
  {
    let nibble = bitsArray.slice(i, i + 4);
    hex.push(parseInt(nibble.join(""), 2).toString(16));
  }

  return hex.join("");
};

let bmvbhashEven = function(data, bits)
{
  let blocksizeX = Math.floor(data.width / bits);
  let blocksizeY = Math.floor(data.height / bits);

  let result = [];

  for (let y = 0; y < bits; y++)
  {
    for (let x = 0; x < bits; x++)
    {
      let total = 0;

      for (let iy = 0; iy < blocksizeY; iy++)
      {
        for (let ix = 0; ix < blocksizeX; ix++)
        {
          let cx = x * blocksizeX + ix;
          let cy = y * blocksizeY + iy;
          let ii = (cy * data.width + cx) * 4;

          let alpha = data.data[ii + 3];
          if (alpha === 0)
          {
            total += 765;
          }
          else
          {
            total += data.data[ii] + data.data[ii + 1] + data.data[ii + 2];
          }
        }
      }

      result.push(total);
    }
  }

  translateBlocksToBits(result, blocksizeX * blocksizeY);
  return bitsToHexhash(result);
};

let bmvbhash = function(data, bits)
{
  let result = [];

  let i; let j; let x; let y;
  let blockWidth; let blockHeight;
  let weightTop; let weightBottom; let weightLeft; let weightRight;
  let blockTop; let blockBottom; let blockLeft; let blockRight;
  let yMod; let yFrac; let yInt;
  let xMod; let xFrac; let xInt;
  let blocks = [];

  let evenX = data.width % bits === 0;
  let evenY = data.height % bits === 0;

  if (evenX && evenY)
  {
    return bmvbhashEven(data, bits);
  }

  // initialize blocks array with 0s
  for (i = 0; i < bits; i++)
  {
    blocks.push([]);
    for (j = 0; j < bits; j++)
    {
      blocks[i].push(0);
    }
  }

  blockWidth = data.width / bits;
  blockHeight = data.height / bits;

  for (y = 0; y < data.height; y++)
  {
    if (evenY)
    {
      // don't bother dividing y, if the size evenly divides by bits
      blockTop = blockBottom = Math.floor(y / blockHeight);
      weightTop = 1;
      weightBottom = 0;
    }
    else
    {
      yMod = (y + 1) % blockHeight;
      yFrac = yMod - Math.floor(yMod);
      yInt = yMod - yFrac;

      weightTop = (1 - yFrac);
      weightBottom = (yFrac);

      // yInt will be 0 on bottom/right borders and on block boundaries
      if (yInt > 0 || (y + 1) === data.height)
      {
        blockTop = blockBottom = Math.floor(y / blockHeight);
      }
      else
      {
        blockTop = Math.floor(y / blockHeight);
        blockBottom = Math.ceil(y / blockHeight);
      }
    }

    for (x = 0; x < data.width; x++)
    {
      let ii = (y * data.width + x) * 4;

      let avgvalue; let alpha = data.data[ii + 3];
      if (alpha === 0)
      {
        avgvalue = 765;
      }
      else
      {
        avgvalue = data.data[ii] + data.data[ii + 1] + data.data[ii + 2];
      }

      if (evenX)
      {
        blockLeft = blockRight = Math.floor(x / blockWidth);
        weightLeft = 1;
        weightRight = 0;
      }
      else
      {
        xMod = (x + 1) % blockWidth;
        xFrac = xMod - Math.floor(xMod);
        xInt = xMod - xFrac;

        weightLeft = (1 - xFrac);
        weightRight = xFrac;

        // xInt will be 0 on bottom/right borders and on block boundaries
        if (xInt > 0 || (x + 1) === data.width)
        {
          blockLeft = blockRight = Math.floor(x / blockWidth);
        }
        else
        {
          blockLeft = Math.floor(x / blockWidth);
          blockRight = Math.ceil(x / blockWidth);
        }
      }

      // add weighted pixel value to relevant blocks
      blocks[blockTop][blockLeft] += avgvalue * weightTop * weightLeft;
      blocks[blockTop][blockRight] += avgvalue * weightTop * weightRight;
      blocks[blockBottom][blockLeft] += avgvalue * weightBottom * weightLeft;
      blocks[blockBottom][blockRight] +=
        avgvalue * weightBottom * weightRight;
    }
  }

  for (i = 0; i < bits; i++)
  {
    for (j = 0; j < bits; j++)
    {
      result.push(blocks[i][j]);
    }
  }

  translateBlocksToBits(result, blockWidth * blockHeight);
  return bitsToHexhash(result);
};
