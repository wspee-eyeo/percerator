let hashes = {};
let filterSep = "#$#";
let snippet = "hide-if-contains-image-hash"
let abpExtensionId = "";
let blockBits = 8;

chrome.tabs.onActivated.addListener((info) =>
{
  getHashesFromABP();
});

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    if (request.action == "getFilterText")
    {
      let hostname = extractHostname(request.url);

      sendResponse({filterText: getFilterText(hostname)});

      if (request.clear)
        clearHashesg(hostname);
    }
    else if (request.action == "getHashesFromABP")
    {
      getHashesFromABP();
    }
    else if (request.action == "refreshSettings")
    {
      refreshSettings();
    }
});

function refreshSettings()
{
  chrome.storage.sync.get(null, (data) =>
  {
    if (data && data.abpExtensionId)
      abpExtensionId = data.abpExtensionId;
    if (data && data.blockBits)
      blockBits = data.blockBits;

    getHashesFromABP();
  });
}
refreshSettings();

function clearHashesg(hostname)
{
  delete hashes[hostname];

  chrome.runtime.sendMessage(abpExtensionId, {
    type: "subscriptions.get",
    special: true
  },
  subscriptions =>
  {
    if (subscriptions == undefined)
      subscriptions = [];

    let filters = [];

    for (let subscription of subscriptions)
    {
      for (let filter of subscription.filters)
      {
        if (!filter.text.startsWith(hostname + filterSep + snippet))
        {
          filters.push(filter.text);
        }
      }
    }

    chrome.runtime.sendMessage(abpExtensionId, {
      type: "filters.importRaw",
      text: filters.join("\n"),
      removeExisting: true
    },
    errors =>
    {
      if (errors && errors.length)
        console.log(errors);
    });
  });
}

function addFilterToABP(hostname)
{
  let filterText = getFilterText(hostname);

  chrome.runtime.sendMessage(abpExtensionId, {
    type: "subscriptions.get",
    special: true
  },
  subscriptions =>
  {
    if (subscriptions == undefined)
      subscriptions = [];

    let filters = [];

    for (let subscription of subscriptions)
    {
      for (let filter of subscription.filters)
      {
        if (!filter.text.startsWith(hostname + filterSep + snippet))
        {
          filters.push(filter.text);
        }
      }
    }

    filters.push(filterText);

    chrome.runtime.sendMessage(abpExtensionId, {
      type: "filters.importRaw",
      text: filters.join("\n"),
      removeExisting: true
    },
    errors =>
    {
      if (errors && errors.length)
        console.log(errors);
    });
  });
}

function getFilterText(hostname) {
  let result = "";

  if (hashes[hostname] && hashes[hostname].length)
  {
    result += hostname + filterSep + snippet + " ";
    result += hashes[hostname].join(',');

    if (blockBits != 8)
       result += " img 0 " + blockBits;
  }
  return result;
}

function extractHostname(url) {
    var hostname;
    //find & remove protocol (http, ftp, etc.) and get hostname

    if (url.indexOf("//") > -1) {
        hostname = url.split('/')[2];
    }
    else {
        hostname = url.split('/')[0];
    }

    //find & remove port number
    hostname = hostname.split(':')[0];
    //find & remove "?"
    hostname = hostname.split('?')[0];

    return hostname;
}

function getHashesFromABP(callback) {
  chrome.runtime.sendMessage(abpExtensionId, {
    type: "subscriptions.get",
    special: true
  },
  subscriptions =>
  {
    if (subscriptions == undefined)
      subscriptions = [];
    else
      for (let key of Object.keys(hashes))
        delete hashes[key]

    let filters = [];

    for (let subscription of subscriptions)
    {
      for (let filter of subscription.filters)
      {
        let [hostname, other] = filter.text.split("#$#");
        if ((other || "").startsWith(snippet))
        {
          for (let hash of (filter.text.split(" ")[1] || "").split(","))
          {
            if (!hashes[hostname])
              hashes[hostname] = [];
            if (hashes[hostname].indexOf(hash) == -1)
              hashes[hostname].push(hash);
          }
        }
      }
    }

    if (callback)
      callback();
  });
}

function addHashg(hash, hostname, callback)
{
  if (!hashes[hostname])
    hashes[hostname] = [];
  if (hashes[hostname].indexOf(hash) == -1)
    hashes[hostname].push(hash);

  if (callback)
    callback();
}

function handleImage(info, tab) {
  let imageElement = new Image();
  imageElement.crossOrigin = "anonymous"
  imageElement.onload = () => {
    let canvas = document.createElement("canvas");
    let context = canvas.getContext("2d");

    let sX = 0;
    let sY = 0;
    let sWidth = imageElement.width;
    let sHeight = imageElement.height;

    canvas.width = sWidth;
    canvas.height = sHeight;

    context.drawImage(
      imageElement, sX, sY, sWidth, sHeight, 0, 0, sWidth, sHeight);

    let imageData = context.getImageData(0, 0, sWidth, sHeight);
    let result = bmvbhash(imageData, blockBits);

    let hostname = extractHostname(info.pageUrl);

    getHashesFromABP(() =>
    {
      addHashg(result, hostname, () =>
      {
        addFilterToABP(hostname);
      });
    });
  }
  imageElement.src = info.srcUrl;
}

let contexts = ["image"];
let id = chrome.contextMenus.create({
  "title": "Add Image",
  "contexts": contexts,
  "onclick": handleImage
});
